var express = require('express');
var router = express.Router();
var _ = require('lodash');



module.exports = router;

//Data
let movies = [ {
    id: '1',
    movie: 'Titanic',
    yearOfRelease: 1993,
    duration: 210, // en minutes,
    actors: ['DiCapRio', 'Kitty'],
    poster: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/RMS_Titanic_3.jpg/280px-RMS_Titanic_3.jpg', // lien vers une image d'affiche,
    boxOffice: 20000000000, // en USD$,
    rottenTomatoesScore: 10
   }
  ,
{
    id: '2',
    movie: 'Titanic2',
    yearOfRelease: 2993,
    duration: 210, // en minutes,
    actors: ['DiCapRioJr', 'KittyJr'],
    poster: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/RMS_Titanic_3.jpg/280px-RMS_Titanic_3.jpg', // lien vers une image d'affiche,
    boxOffice: 500, // en USD$,
    rottenTomatoesScore: 9
}];


/* GET movie listing. */
router.get('/', function(req, res) {
    res.status(200).json({movies});
  });

  /* GET a movie with an ID */
router.get('/:id', (req, res) =>
{
    const {id} = req.params;
    const amovie = _.find(movies,["id",id]);
    res.status(200).json({message : "Movie found !", amovie});
  });

    /* GET one movie listing in a .json */
router.get('/', function(req, res) {
    res.status(200).json({movies});
  });
/*ADD*/
  router.put('/',(req, res)=>
  {
const {amovie} = req.body;
const id = _.uniqueId();
movies.push({amovie, id});
res.json(
    {
message : `Juste added ${id}`,
user: {amovie, id}
    }
);
  });
/*Update*/
  router.post('/:id',(req, res) =>
  {
    const {id} = req.params;
    const {amovie} = req.body;
    const movieToUpdate = _.find(movies,["id", id]);
    movieToUpdate.amovie = amovie;
    
    res.json({message: `Updated ${id} `});
  });
/*Delete*/
  router.delete('/:id',(req,res) =>
  {
    const {id} = req.params;
    _.remove(movies,["id",id]);
    
    res.json({message: `Deleted ${id} `});
  });